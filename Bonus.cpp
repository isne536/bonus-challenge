#include <iostream>

using namespace std;

void quicksort(int arr[],int,int,int);
void merge(int arr[],int,int,int);
void mergesort(int arr[],int,int);

int main()
{
	int arr[10] = {1,4,6,7,3,2,9,10,5,8};	// creating an array of integers.
	int left,right,m;

	quicksort(arr,10,left,right);
	for (int i=0; i<10; i++)
	{
		cout << arr[i] << " ";
	}
	
	mergesort(arr,10,m);
	for(int j=0; j<10; j++)
	{
		cout << arr[j] <<" ";
	}
	
	system("pause");
	return 0;
}

void quickSort(int arr[], int left, int right) 
{      
	int i = left, j = right;
    int tmp;
    int pivot = arr[(left + right) / 2];

    /* partition */

    while (i <= j) 
	{
	    while (arr[i] < pivot)
	        i++;
	        while (arr[j] > pivot)
                j--;

		if (i <= j)
		{
	        tmp = arr[i];
			arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
    };

    /* recursion */

   if (left < j)
	{
    	quickSort(arr, left, j);
	}
   	
	   if (i < right)
   	{
       	quickSort(arr, i, right);
	}
}	


void merge(int arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = (m - l)+1;
    int n2 = r - m;
    /* create temp arrays */
    int L[n1], R[n2];
 
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];
 
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }
 
    /* Copy the remaining elements of L[], if there
       are any */
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }
 
    /* Copy the remaining elements of R[], if there
       are any */
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}
 
/* l is for left index and r is right index of the
   sub-array of arr to be sorted */
void mergeSort(int arr[], int l, int r)
{
    if (l < r)
    {
        // Same as (l+r)/2, but avoids overflow for
        // large l and h
        int m = l+(r-l)/2;
 
        // Sort first and second halves
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
 
        merge(arr, l, m, r);
    }
}
